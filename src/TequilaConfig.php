<?php
/**
 * Created by PhpStorm.
 * User: loichu
 * Date: 07.02.19
 * Time: 11:59
 */

namespace EPFL\Tequila;


class TequilaConfig
{
    # The Tequila server URL
    private static $tequilaURL = 'https://tequila.epfl.ch/cgi-bin/tequila';

    # The timeout for the session (in seconds)
    private static $sessionTimeout = 86400; // 24 hours

    # Application logout URL
    private static $logoutURL = 'https://localhost/tequila/logout.php';

    public static function GetOption($option = '')
    {
        try {
            $return = self::$$option;
        } catch (\Exception $exception) {
            $return = '';
        }
        return $return;
    }

    public static function SetOption($option, $value = null)
    {
        try {
            self::$$option = $value;
        } catch (\Exception $exception) {
            return false;
        }
    }
}