<?php
/**
 * Created by PhpStorm.
 * User: loichu
 * Date: 07.02.19
 * Time: 12:35
 */

// Constants declarations
define('SESSION_INVALIDKEY', 8);
define('SESSION_READ', 9);
define('SESSION_TIMEOUT', 7);

define('AUTHENTICATE_RSA', 1); // No longer used, ignored.
define('AUTHENTICATE_URL', 2); // No longer used, ignored.

define('ERROR_AUTH_METHOD_UNKNOWN', 129);
define('ERROR_CREATE_FILE', 134);
define('ERROR_CREATE_SESSION_DIR', 137);
define('ERROR_CREATE_SESSION_FILE', 145);
define('ERROR_NO_DATA', 133);
define('ERROR_NO_KEY', 131);
define('ERROR_NO_MESSAGE', 139);
define('ERROR_NO_SERVER_DEFINED', 143);
define('ERROR_NO_SERVER_KEY', 140);
define('ERROR_NO_SESSION_DIR', 130);
define('ERROR_NO_SIGNATURE', 138);
define('ERROR_NO_VALID_PUBLIC_KEY', 141);
define('ERROR_NOT_READABLE', 135);
define('ERROR_SESSION_DIR_NOT_WRITEABLE', 148);
define('ERROR_SESSION_FILE_EXISTS', 144);
define('ERROR_SESSION_FILE_FORMAT', 146);
define('ERROR_SESSION_TIMEOUT', 136);
define("ERROR_UNKNOWN_ERROR", 127);
define('ERROR_UNSUPPORTED_METHOD', 132);
define('ERROR_CURL_NOT_LOADED', 149);

define('LNG_DEUTSCH', 2);
define('LNG_ENGLISH', 1);
define('LNG_FRENCH', 0);

define('COOKIE_LIFE', 86400);
define('COOKIE_NAME', 'TequilaPHP');
define('MIN_SESSION_TIMEOUT', 600);